package model

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var DB *gorm.DB

func init() {
	var err error
	DB, err = gorm.Open("mysql", "root:Redline9@/inv_encanto?charset=utf8&parseTime=True")
	if err != nil {
		panic(err)
	}
	DB.AutoMigrate(&Invitation{}, &Category{}, &Image{}, &Cart{}, &CartItem{})
}
