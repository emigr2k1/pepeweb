package model

import (
	"errors"
	"time"
)

type Cart struct {
	ID          int
	CustomerID  string
	CreatedDate time.Time
	CartItems   []CartItem
}

var (
	ErrNoCart = errors.New("model cart: no cart matching")
)

func CartByCustomer(custID string) (Cart, error) {
	custCart := Cart{}
	DB.Preload("CartItems.Invitation.Images").Preload("CartItems.Invitation.Category").Preload("CartItems").Where(&Cart{CustomerID: custID}).First(&custCart)
	if custCart.ID == 0 {
		return custCart, ErrNoCart
	}
	return custCart, nil
}
