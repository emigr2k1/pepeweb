package model

import (
	"strings"
)

type Category struct {
	ID   int
	Name string
}

func GetAllCategories() []Category {
	categories := []Category{}
	DB.Find(&categories)
	return categories
}

func CategoryExists(category string) bool {
	category = strings.ToLower(category)
	categories := GetAllCategories()
	var categoryExists bool
	for _, v := range categories {
		v.Name = strings.ToLower(v.Name)
		if category == v.Name {
			categoryExists = true
			break
		}
	}
	return categoryExists
}

func CategoryByName(name string) (Category, bool) {
	var category Category
	DB.Where(&Category{Name: name}).First(&category)
	if category.ID == 0 {
		return category, false
	}
	return category, true
}
