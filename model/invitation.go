package model

type Invitation struct {
	ID         int
	Name       string
	ShortDesc  string
	LongDesc   string
	Images     []Image
	Category   Category
	CategoryID int
	Price      int
}

func GetInvitations(offset, limit int, category Category) []Invitation {
	var invitations []Invitation
	DB.Where(&Invitation{CategoryID: category.ID}).Offset(offset).Limit(limit).Find(&invitations)
	AssocImagesToInvs(&invitations)
	return invitations
}

func AssocImagesToInvs(invs *[]Invitation) {
	for i, _ := range *invs {
		err := DB.Model(&(*invs)[i]).Order("priority desc").Related(&(*invs)[i].Images).Error
		if err != nil {
			panic(err)
		}
	}
}
