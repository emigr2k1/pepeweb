package model

import (
	"time"
)

type CartItem struct {
	ID           int
	InvitationID int
	Quantity     int
	CartID       int
	CreatedDate  time.Time
	Invitation   Invitation
}

func (c *CartItem) Total() int {
	return c.Quantity * c.Invitation.Price
}

func AddItemToCustCart(custID string, item CartItem, overrideQuantity bool) error {
	custCart := Cart{}
	DB.Where(&Cart{CustomerID: custID}).First(&custCart)
	if custCart.ID == 0 {
		custCart.CustomerID = custID
		custCart.CreatedDate = time.Now()
		DB.Create(&custCart)
	}
	existentItem := CartItem{}
	DB.Where(&CartItem{CartID: custCart.ID, InvitationID: item.InvitationID}).First(&existentItem)
	if existentItem.ID != 0 {
		if overrideQuantity {
			existentItem.Quantity = item.Quantity
		} else {
			existentItem.Quantity++
		}
		DB.Save(&existentItem)
	} else {
		item.CartID = custCart.ID
		DB.Create(&item)
	}
	return nil
}

func RemoveItemFromCusCart(custID string, item CartItem) error {
	custCart, err := CartByCustomer(custID)
	if err != nil {
		return err
	}
	err = DB.Where(&CartItem{CartID: custCart.ID}).Delete(item).Error
	return err
}
