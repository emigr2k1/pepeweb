package model

type Image struct {
	ID           int
	URL          string
	Priority     int
	InvitationID int `gorm:"index"`
}
