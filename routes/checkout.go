package routes

import (
	"gitlab.com/emigr2k1/pepeweb/model"
	"net/http"

	"github.com/gin-gonic/gin"
)

func infoGET(ctx *gin.Context) {
	custID := ctx.Query("token")
	custIDCookie, err := ctx.Cookie("customerID")
	if err != nil {
		ctx.Status(http.StatusForbidden)
		return
	}
	if custID != custIDCookie {
		ctx.Status(http.StatusForbidden)
		return
	}
	custCart, err := model.CartByCustomer(custID)
	if err != nil {
		ctx.Status(http.StatusForbidden)
		return
	}
	ctx.Set("cart", custCart)
	ctx.HTML(http.StatusOK, "checkout/info", ctx.Keys)
}
