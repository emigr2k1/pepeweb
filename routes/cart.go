package routes

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/emigr2k1/pepeweb/model"
)

func cartGET(ctx *gin.Context) {
	custID, err := ctx.Cookie("customerID")
	if err != nil {
		ctx.HTML(http.StatusOK, "cart/index", gin.H{})
		return
	}
	custCart, err := model.CartByCustomer(custID)
	if err != nil {
		ctx.HTML(http.StatusOK, "cart/index", gin.H{})
		fmt.Println(err)
		return
	}
	ctx.Set("cart", custCart)
	ctx.HTML(http.StatusOK, "cart/index", ctx.Keys)
}

func cartPOST(ctx *gin.Context) {
	custID, err := ctx.Cookie("customerID")
	if err != nil {
		ctx.Status(http.StatusBadRequest)
		return
	}
	invID, err := strconv.Atoi(ctx.PostForm("invitationID"))
	if err != nil {
		ctx.Status(http.StatusBadRequest)
		return
	}
	invQuantity, err := strconv.Atoi(ctx.PostForm("quantity"))
	if err != nil {
		invQuantity = 1
	}
	cartItem := model.CartItem{
		CreatedDate:  time.Now(),
		InvitationID: invID,
		Quantity:     invQuantity,
	}
	err = model.AddItemToCustCart(custID, cartItem, false)
	if err != nil {
		ctx.Status(http.StatusInternalServerError)
		return
	}
	ctx.Status(http.StatusOK)
}

func cartUpdate(ctx *gin.Context) {
	custID, exists := ctx.GetPostForm("token")
	if !exists || custID == "" {
		ctx.Status(http.StatusForbidden)
		return
	}
	custIDCookie, err := ctx.Cookie("customerID")
	if err != nil {
		ctx.Status(http.StatusForbidden)
		return
	}
	if custID != custIDCookie {
		ctx.Status(http.StatusForbidden)
		return
	}
	itemID, err := strconv.Atoi(ctx.PostForm("itemID"))
	if err != nil {
		ctx.Status(http.StatusBadRequest)
		return
	}
	quantity, err := strconv.Atoi(ctx.PostForm("quantity"))
	if err != nil {
		ctx.Status(http.StatusBadRequest)
		return
	}
	item := model.CartItem{
		ID:          itemID,
		CreatedDate: time.Now(),
		Quantity:    quantity,
	}
	err = model.AddItemToCustCart(custID, item, true)
	if err != nil {
		ctx.Status(http.StatusBadRequest)
		return
	}
}

func cartDelete(ctx *gin.Context) {
	custID := ctx.PostForm("token")
	if custID == "" {
		ctx.Status(http.StatusBadRequest)
		return
	}
	itemID, err := strconv.Atoi(ctx.PostForm("itemID"))
	if err != nil {
		ctx.Status(http.StatusBadRequest)
		return
	}
	item := model.CartItem{
		ID: itemID,
	}
	err = model.RemoveItemFromCusCart(custID, item)
	if err != nil {
		ctx.Status(http.StatusForbidden)
	}
}
