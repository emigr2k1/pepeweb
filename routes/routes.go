package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/emigr2k1/pepeweb/middleware"
)

func RegisterRoutes(router *gin.Engine) {
	router.Use(middleware.Customer)
	router.GET("/", indexGET)
	shop := router.Group("/tienda")
	{
		shop.GET("/:category", shopCategoryGET)
	}
	cart := router.Group("/carrito", middleware.CSRFToken)
	{
		cart.GET("/", cartGET)
		cart.POST("/", cartPOST)
		cart.POST("/actualizar", cartUpdate)
		cart.POST("/borrar", cartDelete)
	}
	checkout := router.Group("/pagar")
	{
		checkout.GET("/info", infoGET)
	}
}

func indexGET(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "home/index", gin.H{})
}
