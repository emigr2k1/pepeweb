package routes

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/emigr2k1/pepeweb/model"
)

func shopGET(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "shop/index", gin.H{})
}

func shopCategoryGET(ctx *gin.Context) {
	categoryParam := ctx.Param("category")
	pageQuery := ctx.Query("page")
	page, err := strconv.Atoi(pageQuery)
	if err != nil {
		page = 1
	}
	category, found := model.CategoryByName(categoryParam)
	if !found {
		ctx.Status(http.StatusNotFound)
		return
	}
	invitations := model.GetInvitations((page-1)*25, 25, category)
	ctx.HTML(http.StatusOK, "shop/category", gin.H{
		"invitations": invitations,
		"title":       categoryParam,
	})
}
