package main

import (
	"html/template"

	"github.com/gin-gonic/gin"
	"gitlab.com/emigr2k1/pepeweb/routes"
)

func main() {
	app := gin.Default()
	app.SetFuncMap(template.FuncMap{
		"set": func(viewArgs map[string]interface{}, key string, value interface{}) interface{} {
			viewArgs[key] = value
			return nil
		},
	})
	app.LoadHTMLGlob("view/**/*")
	app.Static("/static", "./static")
	routes.RegisterRoutes(app)
	app.Run(":1111")
}
