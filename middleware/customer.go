package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/emigr2k1/pepeweb/session"
)

func Customer(ctx *gin.Context) {
	_, err := ctx.Cookie("customerID")
	if err == http.ErrNoCookie {
		session, err := session.GenerateStr(36)
		if err != nil {
			panic(err)
		}
		ctx.SetCookie("customerID", session, 0, "/", "", false, false)

	}
}
