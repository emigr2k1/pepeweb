package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func CSRFToken(ctx *gin.Context) {
	custID, err := ctx.Cookie("customerID")
	if err == http.ErrNoCookie {
		ctx.Status(http.StatusBadRequest)
		return
	}
	ctx.Set("CSRFToken", custID)
}
