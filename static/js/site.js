$(document).ready(function() {
	fixFooter();
	$(window).resize(function() {
		fixFooter();
	})
});

function fixFooter() {
	var docHeight = $(window).height();
	var footerHeight = $('.footer').outerHeight();
	var footerTop = $('.footer').position().top + footerHeight;

	if (footerTop < docHeight) {
		$('.footer').css('margin-top', (docHeight - footerTop) + 'px');
	}

}

$("a").click(function(e){
	let link = $(this).attr("href");
	if(link === "#"){
		e.preventDefault();
	}
});
	
$("#scroll-screen").click(function(e){
	let winHeight = $(window).height();
	$('html, body').animate({
		scrollTop: winHeight
	}, 500);
});
